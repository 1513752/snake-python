import pygame
import random


pygame.init()
pygame.init()

font = pygame.font.SysFont(None, 40)
gameDisplayX = 800
gameDisplayY = 600
gameDisplay = pygame.display.set_mode((gameDisplayX, gameDisplayY))
block_size = 10
white = (255, 255, 255)
black = (0, 0, 0)
red = (255, 0, 0)
green = (0, 255, 255)

def snake(snakelist, length):
    snakelist
    for XnY in snakelist:
        pygame.draw.rect(gameDisplay, green, [XnY[0], XnY[1], block_size, block_size])


def text_to_screen(ms, color):
    screen_t = font.render(ms, True, color)
    gameDisplay.blit(screen_t, [200, 200])


def gameLoop():
    gameExit = False
    gameOver = False
    clock = pygame.time.Clock()

    pygame.display.set_caption('Slither')

    lead_x = 0
    lead_y = 0
    change_x = 0
    change_y = 0
    isChangeX = False
    isChangeY = False
    p_X = round(random.randrange(0, gameDisplayX) / 10) * 10
    p_Y = round(random.randrange(0, gameDisplayY) / 10) * 10

    snakelist = []
    length = 1


    while not gameExit:

        gameDisplay.fill(white)
        pygame.draw.rect(gameDisplay, red, [p_X, p_Y, block_size, block_size])


        while gameOver == True:
            text_to_screen("Game over! Press C to continue or press Q to quit", red)
            pygame.display.update()
            for event in pygame.event.get():
                if event.type == pygame.KEYDOWN:
                    print(event.key)
                    if event.key == pygame.K_c:
                        gameOver = False
                        lead_x = 0
                        lead_y = 0
                        isChangeY = False
                        isChangeX = False
                        print(event.key)
                    if event.key == pygame.K_q:
                        gameExit == True



        for event in pygame.event.get():

            if event.type == pygame.QUIT:
                gameExit = True

            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_LEFT:
                    change_x = -10
                    isChangeX = True
                    isChangeY = False
                elif event.key == pygame.K_RIGHT:
                     change_x = 10
                     isChangeX = True
                     isChangeY = False
                elif event.key == pygame.K_UP:
                     change_y = -10
                     isChangeY = True
                     isChangeX = False
                else:
                     change_y = 10
                     isChangeY = True
                     isChangeX = False

        if lead_x > gameDisplayX or lead_x < 0 or lead_y > gameDisplayY or lead_y < 0:
            gameOver = True

        if isChangeX == True:  lead_x += change_x

        if isChangeY == True:  lead_y += change_y

        snakeHead = []
        snakeHead.append(lead_x)
        snakeHead.append(lead_y)
        snakelist.append(snakeHead)

        if len(snakelist) > length:
            del snakelist[0]
        snake(snakelist, length)
        pygame.display.update()


        clock.tick(10+length)
        if lead_x == p_X and lead_y == p_Y:
            p_X = round(random.randrange(0, gameDisplayX) / 10) * 10
            p_Y = round(random.randrange(0, gameDisplayY) / 10) * 10
            length = length + 1



    pygame.quit()
    quit()


gameLoop()